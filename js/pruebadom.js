function cambiarColor(){
    console.log("Hola dentro de la function cambiar color");
    //acceder a un elemento del HTML

    let titulodom = document.getElementById("titulo");
    console.log(titulodom);
    console.log(titulodom.id);
    console.log(titulodom.className);

    //cambiar la clase del objeto del titulodom
    titulodom.className = "text-danger display-4";
    titulodom.innerHTML = "Continuara...."
}

function vermas(){
    //buscar el nodo padre (ahora es el article)
    let articulos = document.getElementsByTagName("article");
    console.log(articulos[0]);
    
    let btnArticulo = document.getElementById("btnOcultar")
    if(btnArticulo.innerHTML =="Ver mas"){

        //Crear un elemento nuevo
    let parrafo = document.createElement("p");

    console.log(parrafo);

    parrafo.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad sunt aspernatur delectus fugit id quibusdam eum dolore fugiat omnis labore quisquam ut voluptatem consectetur nam quo facere accusamus corrupti, quia sed nisi commodi animi laboriosam eligendi fuga! Quae repudiandae id tenetur deleniti facilis hic soluta ipsam odit. Exercitationem, voluptates corrupti?";

    parrafo.className ="lead";

    //agregar el nodo hijo al elemento padre
    articulos[0].appendChild(parrafo);

    
    btnArticulo.innerHTML = "Ver menos";
    btnArticulo.className = "btn btn-primary";
 }
 else{
    btnArticulo.innerHTML = "Ver mas";
    btnArticulo.className = "btn btn-outline-danger";

    //borrar elementos hijos 
    if(articulos[0].hasChildNodes() && articulos[0].children.length >= 3){
        console.log(articulos[0].children);
        articulos[0].removeChild(articulos[0].children[2]);
    }
 }
}

function transformar(){
    let inputTexto = document.getElementById("inputMayuscula").value;
    console.log(inputTexto.toUpperCase());

    let alerta = document.getElementById("alertMensaje");
    alerta.className = "alert alert-success";
    alerta.innerHTML = inputTexto.toUpperCase();

}